//
//  PhotoDetailsViewController.swift
//  Tumblr
//
//  Created by Jinhua on 2/6/18.
//  Copyright © 2018 Jinhua Chen. All rights reserved.
//

import UIKit

class PhotoDetailsViewController: UIViewController {

    var url : URL?
    var image: UIImage!

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if url != nil {
            imageView.af_setImage(withURL: url!)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
