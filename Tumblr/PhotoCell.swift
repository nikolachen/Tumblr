//
//  PhotoCell.swift
//  Tumblr
//
//  Created by Jinhua on 2/2/18.
//  Copyright © 2018 Jinhua Chen. All rights reserved.
//

import UIKit

class PhotoCell: UITableViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
